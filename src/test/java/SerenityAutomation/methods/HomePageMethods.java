package SerenityAutomation.methods;

import SerenityAutomation.pages.HomePage;

public class HomePageMethods extends BaseMethods {

    public static void waitForHomePageLoad() {
        waitUntilVisible(HomePage.signinButton);
    }

    public static void searchUser(String username) {
        clearAndType(HomePage.userSearchField, username);
        waitAndClick(HomePage.userSuggestionList);
    }

}