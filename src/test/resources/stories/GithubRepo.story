Used for Developing

Meta:
@apibaseurl https://api.github.com
@apikey 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
@pagebaseurl https://github.com/
@username dineshjk92

Narrative:
As a QA Engineer
I want to test search module of Google.com page

Scenario: Verify the number of github repos for a given user from UI matches with that of from API
Given I call the github api to fetch user details
Then The response status code should be 200
When I fetch the value of repos_url from the response
When I call the github api to fetch repo details
Then The response status code should be 200
When I visit the github webpage
When I search for the user
Then The user menu details should be displayed
When I navigate to the user profile page
When I click on Repositories tab
Then The number of repos from the webpage should match with that of from API